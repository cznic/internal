module modernc.org/internal

go 1.21

require (
	github.com/edsrzf/mmap-go v1.2.0
	modernc.org/fileutil v1.3.0
	modernc.org/mathutil v1.7.1
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/sys v0.28.0 // indirect
)
